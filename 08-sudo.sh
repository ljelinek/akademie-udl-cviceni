#!/bin/bash

base_dir=$(realpath $(dirname $0))
base_name=$(basename $0)
if [ ! "$SUDO_USER" ]; then
	control="$base_dir/.control.sh $base_name"
else
	control="sudo -u $SUDO_USER $base_dir/.control.sh $base_name"
fi
result="$base_dir/.result.sh $base_name"

STEPS=("sudo" "dosu")


# Count
$control sudo
if [ $? -ne 0 ]; then
	if [ "$(whoami)" != "root" ]; then
		echo "Nejste root! Spusťe tento program pomocí sudo"
		exit 1
	fi
	echo "$SUDO_COMMAND" | grep "bin/su" 1>/dev/null 2>/dev/null
	if [ $? -eq 0 ]; then
		echo "Nejdřív spusťte tento program pomocí sudo ($SUDO_COMMAND)"
		exit 1
	fi
	echo "Výborně!"
	$control sudo TRUE
fi
$control dosu
if [ $? -ne 0 ]; then
	if [ "$(whoami)" != "root" ]; then
		echo "Nejste root! Spusťte tento program poté, co se přepnete na uživatele root"
		exit 1
	fi
	echo "$SUDO_COMMAND" | grep "bin/su" 1>/dev/null 2>/dev/null
	if [ $? -ne 0 ]; then
		echo "Přepněte se pomocí su na root uživatele a potom spusť tento program"
		exit 1
	fi
	echo "Skvěle!"
	echo "Nezapomeňte se přepnout zpět na běžného uživatele."
	$control dosu TRUE
fi

$result
