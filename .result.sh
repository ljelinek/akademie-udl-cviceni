#!/bin/bash

base_dir=$(realpath $(dirname $0))

if [ $# -ne 1 ]; then
	echo "ERROR: Wrong usage!"
	echo "usage: $0 exercies_script"
	exit 1
fi

file="$base_dir/$1"
control="$base_dir/.control.sh $1"

if [ ! -f "$file" ]; then
	echo "ERROR file $file not found!"
	exit 1
fi

grep STEPS $file 1>/dev/null 2>/dev/null
if [ $? -ne 0 ]; then
	echo "ERROR: Steps not found!"
	exit 1
fi
lines=$(grep STEPS $file 2>/dev/null | wc -l 2>/dev/null)
if [ $lines -ne 1 ]; then
	echo "Multiple usage of STEPS!"
	exit 1
fi
eval $(grep STEPS $file)

all_ok=1
for STEP in ${STEPS[@]}; do
	#echo "checking: $STEP"
	$control $STEP
	if [ $? -ne 0 ]; then
		all_ok=0
		break
	fi
done
if [ $all_ok -eq 1 ] ; then
	echo "Celé cvičení úspěšně hotovo."
	figlet -f big OK 
	exit 0
else
	echo "Některé části cvičení ještě nejsou úspěšně splněné."
	figlet -f big ":-("
	exit 1
fi
