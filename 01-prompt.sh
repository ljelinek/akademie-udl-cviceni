#!/bin/bash

base_dir=$(realpath $(dirname $0))
base_name=$(basename $0)
control="$base_dir/.control.sh $base_name"
result="$base_dir/.result.sh $base_name"

STEPS=("hostname" "username" "pwd")

# HOSTNAME
$control hostname
if [ $? -ne 0 ]; then
	echo "Prosím, zadejte Vámi zjištěné jméno počítače (a stiskněte enter): "
	while true; do
		read hostname
		if [ "$hostname" == "$(hostname)" ]; then
			echo "Výborně!"
			$control hostname TRUE
			break
		else
			echo "Špatně, zkuste to prosím znovu: "
		fi
	done
fi

# USERNAME
$control username
if [ $? -ne 0 ]; then
	echo "Prosím, zadejte Vámi zjištěné jméno uživatele (a stiskněte enter): "
	while true; do
		read username
		if [ "$username" == "$USER" ]; then
			echo "Správně!"
			$control username TRUE
			break
		else
			echo "Špatně, zkuste to prosím znovu:"
		fi
	done
fi

# PWD
$control pwd
if [ $? -ne 0 ]; then
	echo "Prosím, zadejte Vámi zjištěný pracovní adresář (a stiskněte enter): "
	while true; do
		read ipwd
		if [ "$ipwd" == "uvod-do-linuxu" ] || [ "$ipwd" == "uvod-do-linuxu/" ] || [ "$ipwd" == "~/uvod-do-linuxu" ] || [ "$ipwd" == "~/uvod-do-linuxu/" ] || [ "$ipwd" == "$PWD" ] || [ "$(realpath $ipwd 2>/dev/null)" == "$PWD" ] ; then
			echo "Skvěle!"
			$control pwd TRUE
			break
		else
			echo "Špatně, zkuste to prosím znovu:"
		fi
	done
fi

$result
