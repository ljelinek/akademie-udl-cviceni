#!/bin/bash

base_dir=$(realpath $(dirname $0))
base_name=$(basename $0)
control="$base_dir/.control.sh $base_name"
result="$base_dir/.result.sh $base_name"

STEPS=("bash")

# Count
$control bash
if [ $? -ne 0 ]; then
    echo "Kde je na disku umístěn program bash?"
    echo "TIP: Kopírujte umístění."
    while true; do
        read bashloc
        if [ $bashloc == "$(which bash)" ]; then
            echo "Správně!"
            $control bash TRUE
            break
        else
            echo "Špatně, zkuste to prosím znovu:"
        fi
    done
fi

$control add_to_path
if [ $? -ne 0 ]; then
    if echo $PATH | tr : "\n" | grep "$HOME/uvod-do-linuxu"; then
        $control add_to_path TRUE
    else
        echo "Přidejte adresář $HOME/uvod-do-linuxu do standardní cesty:"
        echo "  export \"PATH=\$PATH:$HOME/uvod-do-linuxu\""
        echo "A spusťte znovu program: sedmy"
        exit 1
    fi
fi

$result
