#!/bin/bash

base_dir=$(realpath $(dirname $0))

if [ $# -ne 1 ]; then
	echo "Skript pro resetování již vypracovaného úkolu."
	echo "CHYBA: Špatné použití! Příklady:"
	echo "$0 $(basename $base_dir/02*)"
	echo "$0 ALL"
	exit 1
fi

script=$1

if [ "$1" == "ALL" ]; then
	control="$base_dir/.*.control"
	count=$(ls $control | wc -l)
	echo "Nalezeno $count splněných nebo rozpracovaných úkolů."
else
	control="$base_dir/.$script.control"
	if [ ! -f "$control" ]; then
		echo "Chyba: rozpracovanost úkolu $script nenalezena!"
		exit 1
	fi
fi

echo "Jste si jistý, že chcete resetovat úkol(y)  [y/n]"
read answer
if [ "$answer" == "y" ] || [ "$answer" == "yes" ]; then
	rm -f $control
	if [ $? -ne 0 ]; then
		echo "Reset se nezdařil!"
		exit 1
	fi
	echo "Hotovo"
else
	echo "OK, reset přerušen."
fi
