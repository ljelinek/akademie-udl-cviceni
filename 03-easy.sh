#!/bin/bash

base_dir=$(realpath $(dirname $0))
base_name=$(basename $0)
control="$base_dir/.control.sh $base_name"
result="$base_dir/.result.sh $base_name"

STEPS=("copy" "quit")

function ctrl_c() {
	echo ""
	echo "Výtečně!"
	$control quit TRUE
	$result
	exit 0
}

if [ "$(pwd)" != "$base_dir" ]; then
	echo "Chyba! Jste ve špatném adresáři, spusťte tento program pomocí souboru "Cvičení" na ploše"
	exit 1
fi

if [ -d "Gjj" ]; then
	echo "Chyba, omylem jste vytvořili adresář Gjj, prosím vytvořte adresář začínající na Gjj a zbytek názvu doplňte zmáčknutím tabulátoru"
	rm -rf Gjj
fi

# mkdir
dir="GjjgtyTX3T8mlawz30acPnoShU/nova"
if [ ! -d "$dir" ]; then
	echo "Adresář neexistuje."
	echo -e "Vytvořte nový adresář s názvem \"nova\" (bez uvozovek) uvnitř adresáře začínajícího na Gjj"
	echo "Využijte příkaz: mkdir Gjj<TAB>/nova"
	echo "<TAB> značí doplňění názvu adresáře stisknutím klávesy tabulátoru"
	echo "A znovu spusťte tento program příkazem: treti-<TAB>"
	exit 1
fi
echo "Správně!"

# argn
$control copy
if [ $? -ne 0 ]; then
	random="$(openssl rand -base64 5) $(openssl rand -base64 5)/$(openssl rand -base64 5)"
	echo -e "Prosím, zkopírujte si toto slovo: \"$random\" (bez uvozovek), vložte jej (a stiskněte enter):"
  while true; do
    read randomized
    if [ "$random" == "$randomized" ]; then
      echo "Správně!"
      $control copy TRUE
      break
    else
      echo "Špatně, zkuste to prosím znovu:"
    fi
  done
fi


trap ctrl_c INT
$control quit
if [ $? -ne 0 ]; then
	while true; do
		echo -e "Mne můžete jenom přerušit (klávesovou zkratkou), jak?"
		sleep 1
	done
else
	quit_done=1
fi

$result
