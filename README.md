# Úvod do Linuxu

Cvičení ke kurzu "Úvod do Linuxu". Instalují se na počítače účastníků,
aby si mohli procvičit práci v příkazové řádce.

_Níže uvedené skripty je potřeba spouštět včetně cesty (např. `./`)._

## Instalace

1. Spustit skript `install.sh`.
2. Povolit spouštění zástupce na ploše.

## Používání

Jednotlivá cvičení se spouštění přes aliasy nebo jiným způsobem, viz prezentaci.

### Kontrola stavu

Pomocí skriptu `status.sh` lze zkontrolovat, která cvičení jsou již dokončena.

### Reset cvičení

Konkrétní cvičení lze resetovat do stavu před jeho zahájením pomocí skriptu:

`reset.sh <cvičení>`

Všechna cvičení se resetují:

`reset.sh ALL`

### Kontrola instalace

Pomocí skriptu `check.sh` lze zkontrolovat, zda je nainstalováno vše potřebné.

V rámci kontroly lze chybějící součásti doinstalovat pomocí `check.sh install`.
