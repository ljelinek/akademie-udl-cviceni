#!/bin/bash

# Check environment variables
REQUIRED_PACKAGES="git figlet openssl wget sudo coreutils gnome-terminal curl"
EXPECTED_LSB_ID="Ubuntu"
EXPECTED_LSB_RELEASE="18.04"


BASEDIR="$(realpath $(dirname $0))"
INSTALL_MODE=0
if [ $# -ge 1 ]; then
    if [ "$1" == "install" ]; then
        echo "(Install mode)"
        INSTALL_MODE=1
    fi
fi

##
# Check if all required packages are installed.
#
# If they aren't and check is in install mode, install missing packages.
##
check_packages() {
    missing=""

    for package in $REQUIRED_PACKAGES; do
        if ! dpkg --status $package &>/dev/null; then
            missing="$missing $package"
        fi
    done

    if [ "$missing" ]; then
        echo "VAROVÁNÍ:"
        echo "    Chybí následující balíčky:"
        echo "$missing" | sed -r 's# ([^ ]+)*#        - \1\n#g'
        if [ $INSTALL_MODE -eq 1 ]; then
            echo -n "Instaluji ... "
            sudo apt -y install $missing &>/dev/null
            if [ $? -eq 0 ]; then
                echo "OK"
                return 0
            else
                echo "CHYBA!"
                return 1
            fi
        else
            echo "    Nainstalujte chybějící balíčky, například pomocí:"
            echo "        sudo apt install$missing"
            return 1
        fi
    else
        echo "Všechny programy nutné pro správný běh cvičení jsou nainstalovány."
        return 0
    fi
}

##
# Check whether user uses `bash` as his shell.
##
check_shell() {
    if [ "$(which bash)" != "$SHELL" ]; then
        echo
        echo "VAROVÁNÍ:"
        echo "    Nepoužíváte $(which bash) jako svůj defaultní shell."
        echo "    Některá cvičení nemusí fungovat správně,"
        echo "    nebo nebudou fungovat jejich nápovědy."
        return 1
    fi
    return 0
}

##
# Check if repository is installed in $HOME/uvod-do-linuxu.
##
check_basedir() {
    if [ "$BASEDIR" != "$(realpath ~/uvod-do-linuxu)" ]; then
        echo
        echo "VAROVÁNÍ:"
        echo "    Adresář se cvičeními nemá stejné umístění jako na kurzu (~/uvod-do-linuxu)."
        echo "    Zadání cvičení nebude stoprocentně odpovídat."
        return 1
    fi
    return 0
}

##
# Check whether user runs correct lsb release.
##
check_lsb_release() {
    if which lsb_release &>/dev/null; then
        lsb_id=$(lsb_release -s -i)
        lsb_release=$(lsb_release -s -r)
    else
        lsb_id="unknown"
        lsb_release="unknown"
    fi

    if [ "$EXPECTED_LSB_ID" != "$lsb_id" ]; then
        echo
        echo "VAROVÁNÍ:"
        echo "    Nepoužíváte $EXPECTED_LSB_ID. Některá cvičení nemusí fungovat správně."
        return 1
    else
        if [ "$EXPECTED_LSB_RELEASE" != "$lsb_release" ]; then
            echo
            echo "VAROVÁNÍ:"
            echo "    Nepoužíváte verzi $EXPECTED_LSB_ID $EXPECTED_LSB_RELEASE, pro kterou byla cvičení"
            echo "    optimalizována. Některá cvičení nemusí fungovat správně."
            return 1
        fi
    fi
    return 0
}

##
# Check whether bash aliases are installed.
##
check_aliases() {
    comment="CZNICUDL"
    if ! grep "$comment" $HOME/.bashrc &>/dev/null; then
        echo
        echo "VAROVÁNÍ:"
        echo "    Chybí aliasy! Některá cvičení nebudou fungovat!"
        if [ $INSTALL_MODE -eq 1 ]; then
            echo -n "Konfiguruji ... "
            echo "source $BASEDIR/.bash_aliases  # $comment" >> $HOME/.bashrc
            if [ $? -eq 0 ]; then
                echo "OK"
                return 0
            else
                echo "CHYBA!"
                return 1
            fi
        else
            echo "    Opravíte vložením řádku 'source $BASEDIR/.bash_aliases  # $aliases_comment' na konec souboru '~/.bashrc'"
            return 1
        fi
    fi
}

##
# Check whether desktop file is present and correct
##
check_desktop() {
    desktop_dir="$HOME/Plocha"
    if [ -d "$HOME/Desktop" ]; then
        desktop_dir="$HOME/Desktop"
    fi
    desktop_name="uvod-do-linuxu.desktop"
    desktop_path="$desktop_dir/$desktop_name"

    if [ ! -f $desktop_path ]; then
        echo
        echo "VAROVÁNÍ:"
        echo "    Chybí odkaz na ploše!"
        if [ $INSTALL_MODE -eq 1 ]; then
            echo -n "Vytvářím ... "
            cp "$BASEDIR/$desktop_name" "$desktop_path"
            if [ $? -eq 0 ]; then
                echo "OK"
            else
                echo "CHYBA!"
                return 1
            fi
        fi
    fi

    if [ -f "$desktop_path" ]; then
        if grep USERNAME $desktop_path &>/dev/null; then
            echo "Odkaz na ploše pro první cvičení nemá správnou cestu."
            if [ $INSTALL_MODE -eq 1 ]; then
                echo -n "Opravuji ... "
                sed -i "s/USERNAME/$USER/g" $desktop_path
                if [ $? -eq 0 ]; then
                    echo "OK"
                else
                    echo "FAILED!"
                fi
            fi
        else
            echo "Odkaz na ploše existuje a je správný."
        fi
    fi
}

FAILED=0
check_packages || FAILED=1
check_shell
check_basedir
check_lsb_release
check_aliases || FAILED=1
check_desktop || FAILED=1

if [ $FAILED -eq 0 ]; then
    figlet -f big OK
fi
